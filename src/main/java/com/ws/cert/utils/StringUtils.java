package com.ws.cert.utils;


import java.util.HashMap;
import java.util.Map;



public class StringUtils {
	private StringUtils() {
		    throw new IllegalStateException("Utility class");
	}
	public static String replaceString(String word,String value,String template) {
		 Map<String, String> valuesMap = new HashMap<>();
		 valuesMap.put(word, value);

		 StrSubstitutor sub = new StrSubstitutor(valuesMap);
		 
		return sub.replace(template);
	}
	
	public static String replaceString( Map<String, String> valuesMap,String template) {
		 StrSubstitutor sub = new StrSubstitutor(valuesMap);
		
		return sub.replace(template);
	}
	

	
	
}
