package com.ws.cert.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import com.ws.cert.dto.CertRequestDto;

@Component
public class TemplateCertUtil {
	private static final Logger LOG = LoggerFactory.getLogger(TemplateCertUtil.class);
	
	@Value("${template.tempfile}")
	private String tempfile;
	
	@Value("${template.outputfile}")
	private String outputfile;
	
	public void createTemplate(CertRequestDto dto) {
		LOG.info("------- createTemplate");
		String cn = dto.getReqId() + "/" + dto.getTaxId() + "/" + dto.getBranchId();
		String fullname = "(" + dto.getReqId() + ")" + dto.getTaxpayer() + "/" + dto.getBranchId() + "/" + dto.getDept();
		String taxIdName = dto.getTaxId() + "_" + dto.getTaxpayer();
		
		String outtxt = "";
		String srctxt = "";
		//byte[] bytePassphrase = Base64.getDecoder().decode(dto.getPassphrase());
		String passphrase = dto.getPassphrase();//new String(bytePassphrase);//มีสองจุดตอน gen pfx
		
		try {
			srctxt = readUTF8File(tempfile);
		} catch (IOException e1) {
			LOG.error(e1.getMessage(), e1);
		}
		
		if (srctxt != null) {
			outtxt = srctxt.replace("%passphrase%", passphrase)
						   .replace("%cn%", cn)
						   .replace("%email%", dto.getEmail())
						   .replace("%TaxID%", dto.getTaxId())
						   .replace("%taxid-name%",taxIdName)//taxIdName)
						   .replace("%taxpayer%", dto.getTaxpayer())
						   .replace("%regid%", dto.getReqId())
						   .replace("%branch%", dto.getBranchId())
						   .replace("%dept%", dto.getDept())//dto.getDept())
						   .replace("%fullname%", fullname)
						   .replace("%officecode%", dto.getOfficeCode())
						   .replace("%userid%",dto.getUserId());
			
			try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputfile), StandardCharsets.UTF_8))){
				
				writer.write(outtxt);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	public String readUTF8File(String filename) throws IOException {
		String result = null;
					
		InputStream resource = new ClassPathResource(filename).getInputStream();
		try  (BufferedReader reader = new BufferedReader(new InputStreamReader(resource, StandardCharsets.UTF_8))){
			
			result = reader.lines().collect(Collectors.joining("\n"));
	    } catch (Exception e) {
	    	LOG.error(e.getMessage(), e);
	    }
		
		return result;
	}

}
