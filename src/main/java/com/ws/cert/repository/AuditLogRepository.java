package com.ws.cert.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ws.cert.model.AuditLog;
import com.ws.cert.model.Command;

public interface AuditLogRepository extends MongoRepository<AuditLog, Long> {

	AuditLog findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(String requestId, Command subCommand);
	
	AuditLog findFirstByRequestIdCert(String requestIdCert);
}
