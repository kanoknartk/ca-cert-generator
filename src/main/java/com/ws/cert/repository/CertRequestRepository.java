package com.ws.cert.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ws.cert.model.CertRequest;

public interface CertRequestRepository extends MongoRepository<CertRequest, Long> {

}
