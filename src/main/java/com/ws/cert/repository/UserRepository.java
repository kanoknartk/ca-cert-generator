package com.ws.cert.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ws.cert.model.UserInfo;

public interface UserRepository extends MongoRepository<UserInfo, Long> {

}
