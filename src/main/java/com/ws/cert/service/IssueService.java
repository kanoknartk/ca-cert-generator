package com.ws.cert.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ws.cert.dto.CertRequestDto;
import com.ws.cert.dto.DetailDto;
import com.ws.cert.dto.ResponseDto;
import com.ws.cert.model.AuditLog;
import com.ws.cert.model.CertRequest;
import com.ws.cert.model.Command;
import com.ws.cert.model.Event;

import com.ws.cert.repository.AuditLogRepository;
import com.ws.cert.repository.CertRequestRepository;
import com.ws.cert.utils.StringUtils;
import com.ws.cert.utils.TemplateCertUtil;



@Service
public class IssueService {
	private static final Logger LOG = LoggerFactory.getLogger(IssueService.class);
	
	@Value("${path.certout-path}")
	private String certOutFilePath;
	
	@Value("${path.key-path}")
	private String keyPath;
	
	@Value("${path.script-path}")
	private  String scriptPath;
	
	@Value("${hostname.config}")
	private  String hostname;
	
	@Value("${hostname.cacert}")
	private  String cacert;
	
	@Value("${template.outputfile}")
	private String template;
	
	@Value("${command.gen-cer}")
	private String commandGenCer;
	
	@Value("${command.certreq-with-csr}")
	private String certReqWithCsr;
	
	@Value("${command.certutil-resubmit}")
	private String certUtilResubmit;
	
	@Value("${command.openssl-x509}")
	private String opensslX509;
	
	@Value("${command.gen-pfx}")
	private String genPfx;
		
	@Value("${encode.algo}")
	private String algo;
	@Autowired
	private TemplateCertUtil templateUtil;

	private static final String PARAM_MAP_REQUEST_ID = "reqid";
	
	@Autowired
	private AuditLogRepository auditLogRepository;
	@Autowired
	private CertRequestRepository certRequestRepository;
	public ResponseDto issueUSBToken(CertRequestDto[] requestData) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {	
		ResponseDto dto = new ResponseDto(); 
		AuditLog audit = null;
		DetailDto detailDto = null;
		DetailDto[] detailArr = new DetailDto[requestData.length]; 
		for( int i=0 ;i<requestData.length;i++ ) {
			audit = new AuditLog();
			LOG.info(String.format("------- issue reqId : {0}", requestData[i].getReqId()));
			audit.setRequestId(requestData[i].getReqId());
			audit.setCreatedDate(new Date());
			audit.setCommand(Command.ISSUE);
			audit.setSubCommand(Command.ISSUE);
			
			
			
			
			
			
			String cerEncoded =generateCertificateUSB(requestData[i],audit);
			
			audit.setEvent(Event.COMPLETE);
			auditLogRepository.insert(audit);
			DetailDto d = new DetailDto();
			d.setCerBase64(cerEncoded);
			detailArr[i]=d;
		}
		
		dto.setStatus(200);
		dto.setResponseData(detailArr);
		dto.setMessage("Success");
		return dto;
	}
	public String generateCertificateUSB(CertRequestDto requestData,AuditLog audit) throws IOException, InterruptedException {
		Map<String, String> valuesMap = null;
		String command = "";
		String reqIdFromCA = "";
		String outputString = "";
		Process process=null;
		BufferedReader output = null;
		String encoded ="";
		FileUtils.writeByteArrayToFile(new File( keyPath + requestData.getReqId() + "_token.csr"), Base64.getDecoder().decode(requestData.getCsrBase64()));
			valuesMap = new HashMap<>();
			valuesMap.put("hostname", hostname);
			valuesMap.put("csr", keyPath + requestData.getReqId()  + "_token.csr");
//			valuesMap.put("cer", keyPath + reqId + ".cer");
			command = StringUtils.replaceString(valuesMap,certReqWithCsr);
			LOG.info(String.format("------- command certReqWithCsr : {0}"+command, command));
			process = Runtime.getRuntime().exec(command);
		 
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			while ((outputString = output.readLine()) != null) {
				if (outputString.contains("RequestId:")) {
					reqIdFromCA = outputString.substring(outputString.indexOf(':')+1).trim();
				 	break;
				}
			}		 
			
			valuesMap = new HashMap<>();
			valuesMap.put(PARAM_MAP_REQUEST_ID, reqIdFromCA);
			command = StringUtils.replaceString(valuesMap,certUtilResubmit);
			LOG.info(String.format("------- command certUtilResubmit : {0}"+command, command));
			process = Runtime.getRuntime().exec(command);
			 
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			while ((outputString = output.readLine()) != null) {LOG.info(outputString);}
		
//			AuditLog audit = auditLogRepository.findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(reqId, Command.ISSUE);
			if (audit != null) {
				audit.setRequestIdCert(reqIdFromCA);
//				auditLogRepository.save(audit);
			}
			TimeUnit.SECONDS.sleep(2);
			File sourceFile = new File(certOutFilePath+reqIdFromCA+".cer");
			File destFile = new File(keyPath+requestData.getReqId()+"_token.cer");
			Files.copy(sourceFile.toPath(),destFile.toPath());
			LOG.info("------- copyFileFrom : " + certOutFilePath + reqIdFromCA+".cer");
			LOG.info("------- To : " + keyPath+requestData.getReqId()+"_token.cer");
			TimeUnit.SECONDS.sleep(2);
			InputStream is = new FileInputStream(keyPath+requestData.getReqId()+"_token.cer");
			byte[] f = IOUtils.toByteArray(is);
			encoded = new String(java.util.Base64.getEncoder().encode(f));
			LOG.info("-------encoded : " + encoded);
			return encoded;
	}
	private String genKey(CertRequestDto requestData) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {	
		  byte[] salt = {
				    (byte)0xc7, (byte)0x73, (byte)0x21, (byte)0x8c,
				    (byte)0x7e, (byte)0xc8, (byte)0xee, (byte)0x99
				};  

		   String pin =requestData.getPassphrase();
		   String user =requestData.getUserId();
		   String info =pin+user;
		   SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		   KeySpec spec = new PBEKeySpec(info.toCharArray(), salt, 65536, 256);
		   SecretKey tmp = factory.generateSecret(spec);
		   SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		   byte[] encodeKey =Base64.getEncoder().encode(secret.getEncoded());
		   String base64Key = new String(encodeKey);
		   
		  LOG.info(String.format("------- base64Key :", base64Key));
		return base64Key;
	}
	public ResponseDto issue(CertRequestDto[] requestData) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {	

		
		ResponseDto dto = new ResponseDto(); 
		AuditLog audit = null;
		AuditLog auditReturnKey = null;
		AuditLog[] auditArr = new AuditLog[requestData.length]; 
		for( int i=0 ;i<requestData.length;i++ ) {
			if(requestData[i].getYear().equals("")||requestData[i].getYear().equals("2")) {
			audit = new AuditLog();
			LOG.info(String.format("------- issue reqId : {0}", requestData[i].getReqId()));
			audit.setRequestId(requestData[i].getReqId());
			audit.setCreatedDate(new Date());
			audit.setCommand(Command.ISSUE);
			audit.setSubCommand(Command.ISSUE);
			audit.setEvent(Event.FAIL);
			audit.setPassphrase(requestData[i].getPassphrase());
			String key = genKey(requestData[i]);
			audit.setUserKey(key);
			auditLogRepository.insert(audit);
			
			templateUtil.createTemplate(requestData[i]);
			
			generateCsr(requestData[i]);
			TimeUnit.SECONDS.sleep(3);
			String reqIdFromServer = generateCertificate(requestData[i].getReqId());
			TimeUnit.SECONDS.sleep(2);
			copyFileFromServer(reqIdFromServer);
			auditReturnKey = new AuditLog();
			TimeUnit.SECONDS.sleep(2);
			if(processFileCertificate(requestData[i].getReqId())) {
				auditReturnKey.setFileComplete(true);
			}
			
			auditReturnKey.setUserKey(key);
			auditArr[i]=auditReturnKey;
			}else {
				//powershell Invoke-WebRequest -Uri "http://the_url/"
				//for batch version
				 CertRequestDto parent = requestData[i];
				 CertRequest child = new CertRequest();
				BeanUtils.copyProperties(parent, child); 
				certRequestRepository.insert(child);
				
				
			}
		}
		
		dto.setStatus(200);
		dto.setResponseData(auditArr);
		dto.setMessage("Success");
		return dto;
	}
	public ResponseDto issueFromBatch() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {	

		
		ResponseDto dto = new ResponseDto(); 
		AuditLog audit = null;
		AuditLog auditReturnKey = null;
		
		List<CertRequest> requestData =  certRequestRepository.findAll();
		AuditLog[] auditArr = new AuditLog[requestData.size()]; 
		for( int i=0 ;i<requestData.size();i++ ) {
			
			audit = new AuditLog();
			LOG.info(String.format("------- issue reqId : {0}", requestData.get(i).getReqId()));
			audit.setRequestId(requestData.get(i).getReqId());
			audit.setCreatedDate(new Date());
			audit.setCommand(Command.ISSUE);
			audit.setSubCommand(Command.ISSUE);
			audit.setEvent(Event.FAIL);
			audit.setPassphrase(requestData.get(i).getPassphrase());
			String key = genKey(requestData.get(i));
			audit.setUserKey(key);
			auditLogRepository.insert(audit);
			
			templateUtil.createTemplate(requestData.get(i));
			
			generateCsr(requestData.get(i));
			TimeUnit.SECONDS.sleep(4);
			String reqIdFromServer = generateCertificate(requestData.get(i).getReqId());
			TimeUnit.SECONDS.sleep(3);
			copyFileFromServer(reqIdFromServer);
			auditReturnKey = new AuditLog();
			TimeUnit.SECONDS.sleep(1);
			if(processFileCertificate(requestData.get(i).getReqId())) {
				auditReturnKey.setFileComplete(true);
			}
			
			auditReturnKey.setUserKey(key);
			auditArr[i]=auditReturnKey;
			certRequestRepository.delete(requestData.get(i));
		}
		
		dto.setStatus(200);
		dto.setResponseData(auditArr);
		dto.setMessage("Success");
		return dto;
	}
	
	public void generateCsr(CertRequestDto userDetail) throws IOException {
		if (checkFile(template)) {
			 Map<String, String> valuesMap = new HashMap<>();
			 valuesMap.put("tempPath", template);
			 valuesMap.put("outputPath", keyPath+userDetail.getReqId());
			 String command = StringUtils.replaceString(valuesMap,scriptPath+commandGenCer);	
			 LOG.info(String.format("------- command generateCsr : {0}", command));
//			 Runtime.getRuntime().exec(command);	
			 String outputString = "";
				
				BufferedReader output = null;
			 Process process= Runtime.getRuntime().exec(command);
			 output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
				while ((outputString = output.readLine()) != null) {
					LOG.info(outputString);
				}
			 
		}
	}
	
	public String generateCertificate(String reqId) throws IOException {
		Map<String, String> valuesMap = null;
		String command = "";
		String reqIdFromCA = "";
		String outputString = "";
		Process process=null;
		BufferedReader output = null;
		
		if (checkFile(keyPath + reqId + ".csr")) {
			valuesMap = new HashMap<>();
			valuesMap.put("hostname", hostname);
			valuesMap.put("csr", keyPath + reqId + ".csr");
//			valuesMap.put("cer", keyPath + reqId + ".cer");
			command = StringUtils.replaceString(valuesMap,certReqWithCsr);
			LOG.info(String.format("------- command certReqWithCsr : {0}"+command, command));
			process = Runtime.getRuntime().exec(command);
		 
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			while ((outputString = output.readLine()) != null) {
				if (outputString.contains("RequestId:")) {
					reqIdFromCA = outputString.substring(outputString.indexOf(':')+1).trim();
				 	break;
				}
			}		 
			
			valuesMap = new HashMap<>();
			valuesMap.put(PARAM_MAP_REQUEST_ID, reqIdFromCA);
			command = StringUtils.replaceString(valuesMap,certUtilResubmit);
			LOG.info(String.format("------- command certUtilResubmit : {0}"+command, command));
			process =Runtime.getRuntime().exec(command);
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			while ((outputString = output.readLine()) != null) {
				LOG.info(outputString);
			}	
		
			AuditLog audit = auditLogRepository.findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(reqId, Command.ISSUE);
			if (audit != null) {
				audit.setRequestIdCert(reqIdFromCA);
				auditLogRepository.save(audit);
			}
		}	
		return reqIdFromCA;
	}
	
	public void copyFileFromServer(String reqId) throws IOException, InterruptedException {
		AuditLog audit  = auditLogRepository.findFirstByRequestIdCert(reqId);
		if (checkFile(certOutFilePath+reqId+".cer")) {
			File sourceFile = new File(certOutFilePath+reqId+".cer");
			File destFile = new File(keyPath+audit.getRequestId()+".cer");
			Files.copy(sourceFile.toPath(),destFile.toPath());
			LOG.info("------- copyFileFrom : " + certOutFilePath + reqId + ".cer");
			LOG.info("------- To : " + keyPath + audit.getRequestId() + ".cer");
			TimeUnit.SECONDS.sleep(5);
			generatePfx(audit.getRequestId());	
		}
	}

	private void generatePfx(String reqId) throws IOException {
		Map<String, String> valuesMap = null;
		String command = "";
		AuditLog audit  = auditLogRepository.findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(reqId, Command.ISSUE);
		Process process=null;
		BufferedReader output = null;
		 String outputString = "";
		//byte[] bytePassphrase = Base64.getDecoder().decode(audit.getPassphrase());
		String passphrase = audit.getPassphrase();//new String(bytePassphrase);//มีอีกจุดตอนทำ temp
	
		if (checkFile(keyPath+reqId+".cer")) {
			valuesMap = new HashMap<>();
			valuesMap.put("keyPath", keyPath);
			valuesMap.put(PARAM_MAP_REQUEST_ID, reqId);
			command = StringUtils.replaceString(valuesMap,scriptPath+opensslX509);
			LOG.info(String.format("------- command opensslX509 : {0}", command));
			  process=Runtime.getRuntime().exec(command);
			
				
				
				 output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
					while ((outputString = output.readLine()) != null) {
						LOG.info(outputString);
					}
			valuesMap = new HashMap<>();
			valuesMap.put("keyPath", keyPath);
			valuesMap.put(PARAM_MAP_REQUEST_ID, reqId);
			valuesMap.put("CAcert", cacert);
			valuesMap.put("passphrase", passphrase);
			command = StringUtils.replaceString(valuesMap,scriptPath+genPfx);
			LOG.info(String.format("------- command genPfx : {0}", command));
			 process=Runtime.getRuntime().exec(command); 
			
				 output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
					while ((outputString = output.readLine()) != null) {
						LOG.info(outputString);
					}
		}
	}

	public boolean processFileCertificate(String reqId) {
		boolean chk =false;
		File cerFile = new File(keyPath + reqId + ".cer");
		File pfxFile = new File(keyPath + reqId + ".pfx");
		
		if (cerFile.exists() && pfxFile.exists()) {
			AuditLog audit = auditLogRepository.findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(reqId, Command.ISSUE);
			audit.setEvent(Event.COMPLETE);
			audit.setPassphrase(null);
			auditLogRepository.save(audit);
			chk= true;
			LOG.info("------- checkFile complete ---------");
		}
		return chk;
	}
	
	private boolean checkFile(String path) {
		boolean fileFound = false;
		File file = new File(path);
		
		if(file.exists()) {
			fileFound = true;
		}
		
		return fileFound;
	}
}
