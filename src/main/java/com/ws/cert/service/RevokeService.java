package com.ws.cert.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ws.cert.dto.CertRequestDto;
import com.ws.cert.dto.CertResponseDto;
import com.ws.cert.dto.ResponseDto;
import com.ws.cert.model.AuditLog;
import com.ws.cert.model.Command;
import com.ws.cert.model.Event;
import com.ws.cert.repository.AuditLogRepository;
import com.ws.cert.utils.StringUtils;

@Service
public class RevokeService {
	private static final Logger LOG = LoggerFactory.getLogger(RevokeService.class);
	@Value("${path.key-path}")
	private String keyPath;
	
	@Value("${command.find-serialnumber}")
	private String findSerialNumber;
	
	@Value("${command.valid-serialnumber}")
	private String validSerialnumber;
	
	@Value("${command.revoke-cert}")
	private String revokeCert;
	
	@Autowired
	private AuditLogRepository auditLogRepository;
	
	public ResponseDto revoke(CertRequestDto[] requestData) throws IOException {
		ResponseDto dto = new ResponseDto(); 
		ArrayList<CertResponseDto> cerArr = new ArrayList<CertResponseDto>();
		for( int i=0 ;i<requestData.length;i++ ) {
			CertResponseDto certResponse = new  CertResponseDto();
			certResponse.setReqid(requestData[i].getReqId());
			if (verifyCertificate(requestData[i],certResponse)) {
				revokeCertificate(requestData[i],certResponse);
			} else {
				AuditLog audit = new AuditLog();
				audit.setDesc(certResponse.getResponseMsg());
				audit.setRequestId(requestData[i].getReqId());
				audit.setCreatedDate(new Date());
				audit.setCommand(Command.REVOKE);
				audit.setEvent(Event.FAIL);
				auditLogRepository.insert(audit);
			}
			cerArr.add(certResponse);
		}
		dto.setResponseData(cerArr);
		
		return dto;
	}
	
	public int revokeCertificate(CertRequestDto requestDto,CertResponseDto responseDto) throws IOException {
		Map<String, String> valuesMap = null;
		String command = "";
		AuditLog audit = new AuditLog();
		int success = 0;
		String outputString = "";
		Process process=null;
		boolean isRevoke = false;
		BufferedReader output =null;
		valuesMap = new HashMap<>();
		valuesMap.put("serialNumber", responseDto.getCertSerailNumber());
		valuesMap.put("reasonCode", requestDto.getReasonCode());
		command = StringUtils.replaceString(valuesMap,revokeCert);
		LOG.info("------- command revokeCert : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		
		while ((outputString = output.readLine()) != null) {
			if (outputString.contains("successfully")) {
				 isRevoke=true;break;
			}
		}
		
		if (isRevoke) {
			responseDto.setResponseMsg("Revoke Certificate Success.");
			audit.setRequestId(requestDto.getReqId());
			audit.setCreatedDate(new Date());
			audit.setCommand(Command.REVOKE);
			audit.setEvent(Event.COMPLETE);
			auditLogRepository.insert(audit);
			LOG.info("Revoke Certificate Success. ");
			deleteCertificate(requestDto.getReqId()) ;//delete file .cer .pfx , .csr
		} else {
			audit.setRequestId(requestDto.getReqId());
			audit.setCreatedDate(new Date());
			audit.setCommand(Command.REVOKE);
			audit.setEvent(Event.FAIL);
			auditLogRepository.insert(audit);
			LOG.info("Revoke Certificate Faild.");
			responseDto.setResponseMsg("Revoke Certificate Faild.");
		}
		
		return success;
	}
	
	public boolean verifyCertificate (CertRequestDto userDetail,CertResponseDto certResponse) throws IOException {
		Map<String, String> valuesMap = null;
		String command = "";
		String outputString = "";
		Process process=null;
		BufferedReader output =null;
		AuditLog audit = auditLogRepository.findFirstByRequestIdAndSubCommandOrderByCreatedDateDesc(userDetail.getReqId(), Command.ISSUE);
		valuesMap = new HashMap<>();
		valuesMap.put("reqid", audit.getRequestIdCert());
		command = StringUtils.replaceString(valuesMap,findSerialNumber);
		LOG.info("------- command findSerialNumber : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		
		while ((outputString = output.readLine()) != null) {
			 if (outputString.contains("Serial Number:")) {
				 certResponse.setCertSerailNumber(outputString.substring(outputString.indexOf(':')+1).trim());
			 }
		}
		
		valuesMap = new HashMap<>();
		valuesMap.put("serialNumber", certResponse.getCertSerailNumber());
		command = StringUtils.replaceString(valuesMap,validSerialnumber);
		LOG.info("------- command validSerialnumber : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		while ((outputString = output.readLine()) != null) {
			 if (outputString.contains(certResponse.getCertSerailNumber())) {
				 if(outputString.contains("is revoked")) {
					 certResponse.setResponseMsg("Certificate is revoked.");
					 return false;
				 } else if(outputString.contains("is valid")) {
					 return true;
				 } else {
					 certResponse.setResponseMsg("Certificate is invalid.");
					 return false;
				 }
			 }
		}
		
		return false;
	}
	
	private void deleteCertificate(String reqId) throws IOException {

		Files.delete(Paths.get(keyPath + reqId + ".cer"));

		Files.delete(Paths.get(keyPath + reqId + ".csr"));

		Files.delete(Paths.get(keyPath + reqId + ".pem"));

		Files.delete(Paths.get(keyPath + reqId + "cert.pem"));

//		Files.delete(Paths.get(keyPath + reqId + ".rsp"));		

		Files.delete(Paths.get(keyPath + reqId + ".pfx"));		
	}
}
