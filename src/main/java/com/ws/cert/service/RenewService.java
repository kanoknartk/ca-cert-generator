package com.ws.cert.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ws.cert.dto.CertRequestDto;
import com.ws.cert.dto.CertResponseDto;
import com.ws.cert.dto.DetailDto;
import com.ws.cert.dto.ResponseDto;
import com.ws.cert.model.AuditLog;
import com.ws.cert.model.Command;
import com.ws.cert.model.Event;
import com.ws.cert.repository.AuditLogRepository;
import com.ws.cert.utils.StringUtils;
import com.ws.cert.utils.TemplateCertUtil;

@Service
public class RenewService {
	private static final Logger LOG = LoggerFactory.getLogger(VerifyUSBService.class);
	@Value("${path.key-path}")
	private String keyPath;
	@Value("${path.script-path}")
	private  String scriptPath;
	@Value("${command.extract-pfx}")
	private String extractPfx;
	@Value("${command.extract-cert}")
	private String extractCert;
	@Value("${hostname.cacert}")
	private  String cacert;
	@Value("${command.change-pin}")
	private String changePin;
	private static final String PARAM_MAP_REQUEST_ID = "reqid";
	@Autowired
	private IssueService issueService;

	@Autowired
	private RevokeService revokeService;
	
	@Autowired
	private TemplateCertUtil templateUtil;
	
	@Autowired
	private AuditLogRepository auditLogRepository;
	
	public ResponseDto changePin(CertRequestDto requestData) throws IOException {
		ResponseDto dto = new ResponseDto(); 
		 byte[] content = Base64.getDecoder().decode(requestData.getPfxBase64());
		 
		 FileUtils.writeByteArrayToFile(new File(keyPath+"changePin.pfx"), content);
			Map<String, String> valuesMap = null;
			String command = "";
			String outputString = "";
			Process process=null;
			BufferedReader output =null;
			valuesMap = new HashMap<>();
			valuesMap.put("input", keyPath+"changePin.pfx");
			valuesMap.put("output",keyPath+"pfxData.pem");
			valuesMap.put("passphrase",requestData.getOldPin());

			command = StringUtils.replaceString(valuesMap,scriptPath+extractPfx);
			LOG.info("------- command extractPfx : "+command);
			process = Runtime.getRuntime().exec(command);
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			
			while ((outputString = output.readLine()) != null) {
			}
			
			valuesMap = new HashMap<>();
			valuesMap.put("input", keyPath+"pfxData.pem");
			valuesMap.put("output",keyPath+"certData.pem");
			valuesMap.put("passphrase",requestData.getNewPin());

			command = StringUtils.replaceString(valuesMap,scriptPath+extractCert);
			LOG.info("------- command extractCert : "+command);
			process = Runtime.getRuntime().exec(command);
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			
			while ((outputString = output.readLine()) != null) {
			}
			
			process = Runtime.getRuntime().exec(scriptPath+"openssl x509 -noout -subject -in "+keyPath+"certData.pem");
			LOG.info("------- command readCommonName : "+scriptPath+"openssl x509 -noout -subject -in "+keyPath+"certData.pem");
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			String reqIdFromCert = "";
			while ((outputString = output.readLine()) != null) {
				if (outputString.contains("CN=")) {
					String reqIdFromCertStr = outputString.substring(outputString.indexOf("CN=")+3).trim();
					String CommonNameSplit[] = reqIdFromCertStr.split("/");
					reqIdFromCert =CommonNameSplit[0];
					LOG.info("------- reqIdFromCert : "+reqIdFromCert);
//					System.out.println("CommonNameSplit ="+CommonNameSplit[0]);
				 	break;
				}
			}
			
			valuesMap = new HashMap<>();
			valuesMap.put("keyPath", keyPath);
			valuesMap.put(PARAM_MAP_REQUEST_ID, reqIdFromCert);
			valuesMap.put("CAcert", cacert);
			valuesMap.put("oldPin", requestData.getOldPin());
			valuesMap.put("newPin", requestData.getNewPin());

			command = StringUtils.replaceString(valuesMap,scriptPath+changePin);
			LOG.info(String.format("------- command changePin : ", command));
			process = Runtime.getRuntime().exec(command);
			output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
			
			while ((outputString = output.readLine()) != null) {
			}
			DetailDto pfx = new DetailDto();
			File f = new File(keyPath+reqIdFromCert+".pfx");
			byte[] fileByte = Files.readAllBytes(f.toPath());
			String encoded = new String(Base64.getEncoder().encode(fileByte));
			pfx.setPfxBase64(encoded);
			dto.setResponseData(pfx);
		return dto;
	}
	public ResponseDto renew(CertRequestDto[] requestData) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InterruptedException {
		AuditLog audit = new AuditLog();
		ResponseDto dto = new ResponseDto(); 
		revokeService.revoke(requestData);
		dto = issueService.issue(requestData);
//		ArrayList<CertResponseDto> cerArr = new ArrayList<>();
//		for( int i=0 ;i<requestData.length;i++ ) {
//			CertResponseDto certResponse = new  CertResponseDto();
//			certResponse.setReqid(requestData[i].getReqId());
//			if (revokeService.verifyCertificate(requestData[i],certResponse)) {
//				revokeService.revokeCertificate(requestData[i],certResponse);
//				audit.setRequestId(requestData[i].getReqId());
//				audit.setCreatedDate(new Date());
//				audit.setCommand(Command.RENEW);
//				audit.setSubCommand(Command.ISSUE);
//				audit.setEvent(Event.FAIL);
//				audit.setPassphrase(requestData[i].getPassphrase());
//				auditLogRepository.insert(audit);
//				
//				templateUtil.createTemplate(requestData[i]);
//				issueService.generateCsr(requestData[i]);
//			}
//			cerArr.add(certResponse);
//		}
//		dto.setResponseData(cerArr);
		return dto;
	}
}
