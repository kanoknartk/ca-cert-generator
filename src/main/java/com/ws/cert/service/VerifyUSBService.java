package com.ws.cert.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import com.ws.cert.dto.CertRequestDto;
import com.ws.cert.dto.RequestDto;
import com.ws.cert.dto.ResponseDto;
import com.ws.cert.utils.StringUtils;

@Service
public class VerifyUSBService {
	private static final Logger LOG = LoggerFactory.getLogger(VerifyUSBService.class);
	@Value("${path.verify-path}")
	private String verifyPath;
	@Value("${path.key-path}")
	private String keyPath;
	@Value("${path.script-path}")
	private  String scriptPath;
	@Value("${command.extract-pfx}")
	private String extractPfx;
	@Value("${command.extract-cert}")
	private String extractCert;
	@Value("${command.verify-cert}")
	private String verifyCert;
	@Value("${hostname.cacert}")
	private  String cacert;
	public ResponseDto verifyPayload(RequestDto req)  {
		ResponseDto dto = new ResponseDto(); 
		 try {
			 CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				File certFile = new File("C:/Certificate/keyPath/"+req.getReqId()+".cer");
		        FileInputStream stream = new FileInputStream(certFile);
		        X509Certificate cert = (X509Certificate) certFactory.generateCertificate(stream);

				PublicKey publicKey = cert.getPublicKey();
				String decrypted_msg = decryptText(req.getContent(), publicKey);
				dto.setResponseData(decrypted_msg);
		 }catch(Exception e) {
			 
			 dto.setResponseData(false);
		 }
		return dto;
	}
	public String decryptText(String msg, PublicKey key)
			throws Exception {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(Base64.getDecoder().decode(msg)), "UTF-8");
	}
	public ResponseDto verify(CertRequestDto requestData)  {
		ResponseDto dto = new ResponseDto(); 
		 try {
		 FileUtils.writeByteArrayToFile(new File( "C:\\Certificate\\verify\\encrypt.text"), Base64.getDecoder().decode(requestData.getBase64FileEncrypt()));
		 writePfx("C:\\Certificate\\verify\\encrypt.text",requestData.getKey());
		 if (checkFile("C:\\Certificate\\verify\\tmp.pfx")) {
			 dto.setResponseData(verifyPfx(requestData));
		 }
		 }catch(Exception e) {
			 
			 dto.setResponseData(false);
		 }
		return dto;
	}
	private boolean verifyPfx(CertRequestDto requestData) throws Exception {
		boolean verify = false;
		Map<String, String> valuesMap = null;
		String command = "";
		String outputString = "";
		Process process=null;
		BufferedReader output =null;
		valuesMap = new HashMap<>();
		valuesMap.put("input", verifyPath+"tmp.pfx");
		valuesMap.put("output",verifyPath+"pfx.pem");
		valuesMap.put("passphrase",requestData.getPassphrase());

		command = StringUtils.replaceString(valuesMap,scriptPath+extractPfx);
		LOG.info("------- command extractPfx : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		
		while ((outputString = output.readLine()) != null) {
		}
		
		valuesMap = new HashMap<>();
		valuesMap.put("input", verifyPath+"pfx.pem");
		valuesMap.put("output",verifyPath+"cert.pem");
		valuesMap.put("passphrase",requestData.getPassphrase());

		command = StringUtils.replaceString(valuesMap,scriptPath+extractCert);
		LOG.info("------- command extractCert : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		
		while ((outputString = output.readLine()) != null) {
		}
		valuesMap = new HashMap<>();
		valuesMap.put("rootCer", keyPath+cacert);
		valuesMap.put("verifyCer",verifyPath+"cert.pem");


		command = StringUtils.replaceString(valuesMap,scriptPath+verifyCert);
		LOG.info("------- command verifyCert : "+command);
		process = Runtime.getRuntime().exec(command);
		output  = new BufferedReader(new InputStreamReader(process.getInputStream())); 
		
		while ((outputString = output.readLine()) != null) {
			 if(outputString.contains("OK")) {
				 verify = true;
			 }
		}
		
		return verify;
	}
	private void writePfx(String inputFile,String key ) throws Exception {
		  
		  byte[] keyBytes =Base64.getDecoder().decode(key);

		  SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

		  byte[] buf = new byte[1024];

	        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	        IvParameterSpec ivspec = new IvParameterSpec(iv);

	        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
	       
	        c.init(Cipher.DECRYPT_MODE, keySpec, ivspec);

	        InputStream in=new FileInputStream(inputFile);
	        try(FileOutputStream out = new FileOutputStream(new File("C:\\Certificate\\verify\\tmp.pfx" ))) {
	        	
	            in = new CipherInputStream(in, c);

	            int numRead = 0;

	            while ((numRead = in.read(buf)) >= 0) {
	                out.write(buf, 0, numRead);
	            }
	            
	            out.close();


	        } catch (Exception e) {
		    	LOG.error(e.getMessage(), e);
		    }
	        
	       
	}
	private boolean checkFile(String path) {
		boolean fileFound = false;
		File file = new File(path);
		
		if(file.exists()) {
			fileFound = true;
		}
		
		return fileFound;
	}
	
}
