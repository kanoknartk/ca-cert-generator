package com.ws.cert.monitor;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ws.cert.service.IssueService;

@Component
public class DirMonitoring implements InitializingBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(DirMonitoring.class);
	
	private static final int POLL_INTERVAL = 500;
	
	@Value("${path.key-path}")
	private String keyPath;
	
	@Autowired
	private IssueService issueService;
	
	@PostConstruct
	public void init() throws Exception {
		FileAlterationObserver observer = new FileAlterationObserver(keyPath);
        FileAlterationMonitor monitor = new FileAlterationMonitor(POLL_INTERVAL);
        FileAlterationListener listener = new FileAlterationListenerAdaptor() {
            @Override
            public void onFileCreate(File file) {
//            	try {
//       		 		if (file.getName().contains(".csr")) {
//       		 			if(!file.getName().contains("token"))
//       		 			issueService.generateCertificate(file.getName().substring(0,file.getName().indexOf('.')));	   			
//       		 		} else if(file.getName().contains(".pfx")) {
//       		 			issueService.processFileCertificate(file.getName().substring(0,file.getName().indexOf('.')));
//       		 		}
//            	} catch (IOException e) {
//					LOG.info("Error = " + e.getMessage());
//				}   
            }

            @Override
            public void onFileDelete(File file) {
              //  LOG.debug("File: " + file.getName() + " deleted");
            }

            @Override
            public void onFileChange(File file) {
             //  LOG.debug("File: " + file.getName() + " changed");
            }
        };
        observer.addListener(listener);
        monitor.addObserver(observer);
        monitor.start();
	}

	@Override
	public void afterPropertiesSet() throws Exception {}
}
