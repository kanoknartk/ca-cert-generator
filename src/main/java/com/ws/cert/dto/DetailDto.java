package com.ws.cert.dto;

public class DetailDto {

	String pfxBase64 ="";
	String cerBase64 ="";
	String crlBase64 ="";
	public String getPfxBase64() {
		return pfxBase64;
	}

	public void setPfxBase64(String pfxBase64) {
		this.pfxBase64 = pfxBase64;
	}

	public String getCerBase64() {
		return cerBase64;
	}

	public void setCerBase64(String cerBase64) {
		this.cerBase64 = cerBase64;
	}

	public String getCrlBase64() {
		return crlBase64;
	}

	public void setCrlBase64(String crlBase64) {
		this.crlBase64 = crlBase64;
	}
	
}
