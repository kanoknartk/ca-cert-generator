package com.ws.cert.dto;

public class CertRequestDto {

	private String certificateFlag ="";
	
	private String reqId ="";
	private String userId ="";
	private String officeCode="";

	private String taxId ="";
	
	private String taxpayer ="";
	
	private String branchId ="";
	
	private String dept ="";
	
	private String startDate ="";
	
	private String stopDate ="";

	private String email ="";
	
	private String passphrase ="";
	
	private String reasonCode ="";
	
	private String base64FileEncrypt ="";
	private String pfxBase64="";
	private String csrBase64="";
	private String pin ="";
	private String oldPin ="";
	private String newPin ="";

	private String key ="";
	
	private String x ="";
	
	private String y ="";
	
	private String w ="";
	
	private String h ="";
	private String year ="";

	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getOldPin() {
		return oldPin;
	}
	public void setOldPin(String oldPin) {
		this.oldPin = oldPin;
	}
	public String getNewPin() {
		return newPin;
	}
	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}
	public String getPfxBase64() {
		return pfxBase64;
	}
	public void setPfxBase64(String pfxBase64) {
		this.pfxBase64 = pfxBase64;
	}
	public String getCertificateFlag() {
		return certificateFlag;
	}
	public void setCertificateFlag(String certificateFlag) {
		this.certificateFlag = certificateFlag;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	public String getW() {
		return w;
	}
	public void setW(String w) {
		this.w = w;
	}
	public String getH() {
		return h;
	}
	public void setH(String h) {
		this.h = h;
	}
	public String getReqId() {
		return reqId;
	}
	public String getTaxpayer() {
		return taxpayer;
	}

	public void setTaxpayer(String taxpayer) {
		this.taxpayer = taxpayer;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}


	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStopDate() {
		return stopDate;
	}

	public void setStopDate(String stopDate) {
		this.stopDate = stopDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	public String getBase64FileEncrypt() {
		return base64FileEncrypt;
	}
	public void setBase64FileEncrypt(String base64FileEncrypt) {
		this.base64FileEncrypt = base64FileEncrypt;
	}
	public String getCsrBase64() {
		return csrBase64;
	}
	public void setCsrBase64(String csrBase64) {
		this.csrBase64 = csrBase64;
	}
	
}
