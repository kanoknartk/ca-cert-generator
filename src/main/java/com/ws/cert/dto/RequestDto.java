package com.ws.cert.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class RequestDto {
	private String content;
	private String reqId;
	@Valid
    @NotNull
    protected CertRequestDto[] requestData;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public CertRequestDto[] getRequestData() {
		return requestData;
	}

	public void setRequestData(CertRequestDto[] requestData) {
		this.requestData = requestData;
	}
	
	
}
