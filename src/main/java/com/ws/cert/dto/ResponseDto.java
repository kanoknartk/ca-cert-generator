package com.ws.cert.dto;

public class ResponseDto {
	private static final long serialVersionUID = -1367513287722699633L;

	private int status;	
	
	private String message;
	
	private Object responseData;
	


	public Object getResponseData() {
		return responseData;
	}

	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
