package com.ws.cert.dto;

public class CertResponseDto {

	private String reqid ="";
	private String certSerailNumber ="";
	private String responseCode = "";
	private String responseMsg = "";

	public String getReqid() {
		return reqid;
	}
	public void setReqid(String reqid) {
		this.reqid = reqid;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getCertSerailNumber() {
		return certSerailNumber;
	}
	public void setCertSerailNumber(String certSerailNumber) {
		this.certSerailNumber = certSerailNumber;
	}
	
}
