package com.ws.cert.model;

public enum Command {
	ISSUE, REVOKE, RENEW
}
