package com.ws.cert.model;

public enum Event {
	ACCESS, COMPLETE, FAIL
}
