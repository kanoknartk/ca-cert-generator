package com.ws.cert.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true) 
@Document(collection = "auditLog")
public class AuditLog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6480641203091094778L;

	@Id
	private String logId;
	
	private String requestId;
	private String userId;
	private String userKey;
	private boolean fileComplete;
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	private Command command; // issue, revoke, renew
	
	private Command subCommand; // issue, revoke
	
	private String issuer; // name of owner
	
	private Event event; // access (in controller), complete (after ca server sending response), fail
	
	private String desc;
	
	private String passphrase;
	
	private String requestIdCert;
	
	private Date createdDate;



	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getRequestIdCert() {
		return requestIdCert;
	}

	public void setRequestIdCert(String requestIdCert) {
		this.requestIdCert = requestIdCert;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}

	public Command getSubCommand() {
		return subCommand;
	}

	public void setSubCommand(Command subCommand) {
		this.subCommand = subCommand;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}


	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public boolean isFileComplete() {
		return fileComplete;
	}

	public void setFileComplete(boolean fileComplete) {
		this.fileComplete = fileComplete;
	}
	
	
}
